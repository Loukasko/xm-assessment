start:
	make stop
	docker compose build
	docker compose up -d
	make kafka.create-default-topics

stop:
	docker compose stop

rebuild:
ifndef service
	@echo "service parameter is missing"
	@exit 1
endif
	docker compose stop ${service}
	docker compose build ${service}
	docker compose up -d ${service}

kafka.create-default-topics:
	docker exec kafka opt/bitnami/kafka/bin/kafka-topics.sh --bootstrap-server kafka:9092 --create --if-not-exists --topic company-mutation --partitions 5

kafka.create-topic:
ifndef topic
	@echo "topic parameter is missing"
	@exit 1
endif
ifndef partitions
	@echo "partitions parameter is missing (int)"
	@exit 1
endif
	docker exec kafka opt/bitnami/kafka/bin/kafka-topics.sh --bootstrap-server kafka:9092 --create --if-not-exists --topic ${topic} --partitions ${partitions}

kafka.purge-topic:
ifndef topic
	@echo "topic parameter is missing"
	@exit 1
endif
	partitionCount=$$(docker exec kafka opt/bitnami/kafka/bin/kafka-topics.sh --describe --bootstrap-server localhost:9092 --topic ${topic} | grep -Po '(?<=PartitionCount: )\w*'); \
	docker exec kafka opt/bitnami/kafka/bin/kafka-topics.sh --bootstrap-server kafka:9092 --delete --topic ${topic}; \
	make kafka.create-topic topic="$${topic}" partitions="$${partitionCount}"

generate-mock:
ifndef file
	@echo "file parameter is missing"
	@exit 1
endif
	make test-build
	@docker run --volume "$(PWD)/company-service":/app --workdir /app \
	assessment-test-build /bin/bash -c "mockgen -source=${file} -destination=mocks/${file}"

tests-unit:
	make test-build
	@docker run \
		--rm \
		--volume "$(PWD)/company-service":/app \
		--workdir /app \
		assessment-test-build go test -short -cover -count=1 ./...

tests-all:
	make test-build
	@docker run \
		--rm \
		--volume "$(PWD)/company-service":/app \
		--workdir /app \
		assessment-test-build godotenv -f .env go test ./... -cover -count=1

test-build:
	@docker build \
		--tag assessment-test-build \
		-f company-service/Dockerfile.test ./company-service

lint-run:
	@docker run \
		--rm \
		--volume "$(PWD)/company-service":/app \
		-w /app golangci/golangci-lint:v1.50.1 golangci-lint run -v
