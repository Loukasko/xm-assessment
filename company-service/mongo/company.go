package mongo

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
	"time"
	companyDomain "xm-assessment/company/domain"
)

type companyRepository struct {
	mongoClient *mongo.Client
}

const companyCollection = "company"

func NewCompanyRepository(mongoConnectionString string) companyDomain.CompanyRepositoryInt {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	mongoClient, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoConnectionString))
	if err != nil {
		logrus.Errorf("can't ping mongoDB: %s", err.Error())
	}

	return &companyRepository{mongoClient: mongoClient}
}

func (c companyRepository) Create(company *companyDomain.Company) (string, error) {
	collection := c.mongoClient.Database(os.Getenv("MONGO_DATABASE")).Collection(companyCollection)

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	_, err := collection.InsertOne(ctx, company)
	if err != nil {
		return "", err
	}

	return company.ID, nil
}

func (c companyRepository) Patch(companyID string, company *companyDomain.Company) error {
	filter := bson.M{"_id": companyID}
	change := bson.M{
		"$set": company,
	}

	collection := c.mongoClient.Database(os.Getenv("MONGO_DATABASE")).Collection(companyCollection)

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	results, err := collection.UpdateOne(ctx, filter, change)
	if err != nil {
		return err
	}

	if results.MatchedCount == 0 {
		return &companyDomain.NotFoundError{
			ErrorMessage: fmt.Sprintf("company with id: %s not found", companyID),
		}
	}

	return nil
}

func (c companyRepository) Delete(companyID string) error {
	var err error

	queryParams := bson.M{
		"_id": companyID,
	}

	collection := c.mongoClient.Database(os.Getenv("MONGO_DATABASE")).Collection(companyCollection)

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	results, err := collection.DeleteOne(ctx, queryParams)
	if err != nil {
		return err
	}

	if results.DeletedCount == 0 {
		return &companyDomain.NotFoundError{
			ErrorMessage: fmt.Sprintf("company with id: %s not found", companyID),
		}
	}

	return nil
}

func (c companyRepository) Get(companyID string) (*companyDomain.Company, error) {
	var err error

	queryParams := bson.M{
		"_id": companyID,
	}

	collection := c.mongoClient.Database(os.Getenv("MONGO_DATABASE")).Collection(companyCollection)

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	company := &companyDomain.Company{}

	err = collection.FindOne(ctx, queryParams).Decode(company)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, &companyDomain.NotFoundError{
				ErrorMessage: fmt.Sprintf("company with id: %s was not found", companyID),
			}
		}
		return nil, err
	}

	return company, nil
}
