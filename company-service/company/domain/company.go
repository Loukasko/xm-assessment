package domain

type Company struct {
	ID                string `json:"id,omitempty" bson:"_id,omitempty"`
	Name              string `json:"name,omitempty" bson:"name,omitempty"`
	Description       string `json:"description,omitempty" bson:"description,omitempty"`
	AmountOfEmployees int    `json:"amountOfEmployees,omitempty" bson:"amountOfEmployees,omitempty"`
	IsRegistered      bool   `json:"isRegistered,omitempty" bson:"isRegistered,omitempty"`
	Type              string `json:"type,omitempty" bson:"type,omitempty"`
}

type CompanyRepositoryInt interface {
	Create(company *Company) (string, error)
	Patch(companyID string, company *Company) error
	Delete(companyID string) error
	Get(companyID string) (*Company, error)
}
