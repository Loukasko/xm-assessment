package domain

type NotFoundError struct {
	ErrorMessage string
}

func (err NotFoundError) Error() string {
	return err.ErrorMessage
}
