package presentation

import (
	"context"
	"errors"
	"net/http"
	companyService "xm-assessment/company/service"
)

type BadRequestError struct {
	errorMessage string
}

func (err BadRequestError) Error() string {
	return err.errorMessage
}

func getStatusCodeFromErrorType(err error) int {
	if err == nil {
		return http.StatusOK //warning will be returned if we attempt to write 200 - if we want 200 we should write nothing
	}

	if errors.Is(err, context.DeadlineExceeded) {
		return http.StatusGatewayTimeout
	}

	switch err.(type) {
	case *BadRequestError:
		return http.StatusBadRequest
	case *companyService.NotFoundError:
		return http.StatusNotFound
	}

	return http.StatusInternalServerError
}
