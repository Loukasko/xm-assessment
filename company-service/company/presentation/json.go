package presentation

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
	companyService "xm-assessment/company/service"
	"xm-assessment/helper"
)

type CompanyHttpServer struct {
	CompanyService   companyService.CompanyServiceInt
	ValidatorService helper.Validator
}

type apiResponse struct {
	Company      *companyService.Company `json:"company,omitempty"`
	ID           string                  `json:"ID,omitempty"`
	ErrorMessage string                  `json:"errorMessage,omitempty"`
}

func (httpServer *CompanyHttpServer) GetCompany(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var err error
	var requestParams map[string]string
	var response *apiResponse
	var company *companyService.Company

	defer func() {
		response = &apiResponse{
			Company: company,
		}

		if err != nil {
			w.WriteHeader(getStatusCodeFromErrorType(err))
			// we might write a generic message based on error type in case we should not reveal system errors (ie if a web app is the caller)
			response = &apiResponse{
				ErrorMessage: err.Error(),
			}
			logrus.WithFields(logrus.Fields{
				"requestParams": requestParams,
			}).Error(err.Error())
		}

		responseJSON, _ := json.Marshal(response)
		_, _ = w.Write(responseJSON)
	}()

	companyID := mux.Vars(r)["id"]
	if companyID == "" {
		err = &BadRequestError{
			errorMessage: "id field is required",
		}
		return
	}

	validatedCompanyID, err := uuid.Parse(companyID)
	if err != nil {
		err = &BadRequestError{
			errorMessage: "id field is not valid uuid",
		}
		return
	}

	company, err = httpServer.CompanyService.GetCompany(validatedCompanyID.String())
	if err != nil {
		return
	}

}

func (httpServer *CompanyHttpServer) DeleteCompany(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var err error
	var requestParams map[string]string
	var response *apiResponse

	defer func() {
		if err != nil {
			w.WriteHeader(getStatusCodeFromErrorType(err))
			response = &apiResponse{
				ErrorMessage: err.Error(),
			}
			logrus.WithFields(logrus.Fields{
				"requestParams": requestParams,
			}).Error(err.Error())

			responseJSON, _ := json.Marshal(response)
			_, _ = w.Write(responseJSON)
		}
	}()

	companyID, ok := mux.Vars(r)["id"]
	if !ok || companyID == "" {
		err = &BadRequestError{
			errorMessage: "id field is required",
		}
		return
	}

	validatedCompanyID, err := uuid.Parse(companyID)
	if err != nil {
		err = &BadRequestError{
			errorMessage: "id field is not valid uuid",
		}
		return
	}

	err = httpServer.CompanyService.DeleteCompany(validatedCompanyID.String())
	if err != nil {
		return
	}
}

func (httpServer *CompanyHttpServer) CreateCompany(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var err error
	var requestParams map[string]string
	var response *apiResponse
	var companyID string

	defer func() {
		response = &apiResponse{
			ID: companyID,
		}
		if err != nil {
			w.WriteHeader(getStatusCodeFromErrorType(err))
			response = &apiResponse{
				ErrorMessage: err.Error(),
			}
			logrus.WithFields(logrus.Fields{
				"requestParams": requestParams,
			}).Error(err.Error())
		}

		responseJSON, _ := json.Marshal(response)
		_, _ = w.Write(responseJSON)
	}()

	companyToCreate := &companyService.Company{}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(body, companyToCreate)
	if err != nil {
		err = &BadRequestError{
			errorMessage: err.Error(),
		}
	}

	err = httpServer.ValidatorService.Struct(companyToCreate)
	if err != nil {
		err = &BadRequestError{
			errorMessage: err.Error(),
		}
		return
	}

	companyID, err = httpServer.CompanyService.CreateCompany(companyToCreate)
	if err != nil {
		return
	}

}

func (httpServer *CompanyHttpServer) PatchCompany(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var err error
	var requestParams map[string]string
	var response *apiResponse
	var companyID string

	defer func() {
		response = &apiResponse{}
		if err != nil {
			w.WriteHeader(getStatusCodeFromErrorType(err))
			response = &apiResponse{
				ErrorMessage: err.Error(),
			}
			logrus.WithFields(logrus.Fields{
				"requestParams": requestParams,
			}).Error(err.Error())
		}

		responseJSON, _ := json.Marshal(response)
		_, _ = w.Write(responseJSON)
	}()

	companyID = mux.Vars(r)["id"]
	if companyID == "" {
		err = &BadRequestError{
			errorMessage: "id field is required",
		}
		return
	}

	validatedCompanyID, err := uuid.Parse(companyID)
	if err != nil {
		err = &BadRequestError{
			errorMessage: "id field is not valid uuid",
		}
		return
	}

	companyToPatch := &companyService.Company{}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(body, companyToPatch)
	if err != nil {
		return
	}

	err = httpServer.CompanyService.PatchCompany(validatedCompanyID.String(), companyToPatch)
	if err != nil {
		return
	}
}
