package service

import (
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"testing"
	"xm-assessment/company/domain"
	"xm-assessment/event"
	mock_domain "xm-assessment/mocks/company/domain"
	mock_event "xm-assessment/mocks/event"
)

func TestService_CreateCompany(t *testing.T) {
	ctrl := gomock.NewController(t)
	type fields struct {
		companyRepo   domain.CompanyRepositoryInt
		eventProducer event.ProducerInterface
	}
	type args struct {
		company *Company
	}
	tests := []struct {
		name                string
		fields              fields
		args                args
		wantNotEmpty        bool
		wantErr             assert.ErrorAssertionFunc
		companyRepoRes      string
		companyRepoResError error
		timesEventProduced  int
	}{
		{
			name: "we will mock companyRepo.Create() that returns an error so also the function tested will return error",
			fields: fields{
				companyRepo:   mock_domain.NewMockCompanyRepositoryInt(ctrl),
				eventProducer: mock_event.NewMockProducerInterface(ctrl),
			},
			wantNotEmpty:        false,
			wantErr:             assert.Error,
			companyRepoResError: errors.New("some error"),
		},
		{
			name: "we will mock companyRepo.Create() that returns no error, so CreateCompany will return no errors too",
			fields: fields{
				companyRepo:   mock_domain.NewMockCompanyRepositoryInt(ctrl),
				eventProducer: mock_event.NewMockProducerInterface(ctrl),
			},
			timesEventProduced:  1,
			wantNotEmpty:        true,
			wantErr:             assert.NoError,
			companyRepoResError: nil,
			companyRepoRes:      "any id",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := Service{
				companyRepo:   tt.fields.companyRepo,
				eventProducer: tt.fields.eventProducer,
			}

			s.companyRepo.(*mock_domain.MockCompanyRepositoryInt).
				EXPECT().
				Create(gomock.Any()).
				Times(1).
				Return(tt.companyRepoRes, tt.companyRepoResError)

			s.eventProducer.(*mock_event.MockProducerInterface).
				EXPECT().
				ProduceMessageAsync(gomock.Any(), gomock.Any()).
				Times(tt.timesEventProduced).
				Return()

			got, err := s.CreateCompany(tt.args.company)
			if !tt.wantErr(t, err, fmt.Sprintf("CreateCompany(%v)", tt.args.company)) {
				return
			}
			if tt.wantNotEmpty {
				assert.NotEmpty(t, got, "CreateCompany(%v)", tt.args.company)
			}
		})
	}
}

func TestService_DeleteCompany(t *testing.T) {
	ctrl := gomock.NewController(t)
	type fields struct {
		companyRepo   domain.CompanyRepositoryInt
		eventProducer event.ProducerInterface
	}
	type args struct {
		companyID string
	}
	tests := []struct {
		name                string
		fields              fields
		args                args
		wantErr             assert.ErrorAssertionFunc
		companyRepoResError error
		timesEventProduced  int
	}{
		{
			name: "we will mock companyRepo.Delete() response. We will set that it returns error and " +
				"assert that an error is returned from the function tested ",
			fields: fields{
				companyRepo:   mock_domain.NewMockCompanyRepositoryInt(ctrl),
				eventProducer: mock_event.NewMockProducerInterface(ctrl),
			},
			wantErr:             assert.Error,
			companyRepoResError: errors.New("some other random error"),
		},
		{
			name: "we will mock companyRepo.Delete() response AND eventProducer.ProduceMessageAsync response." +
				" We will set that it returns NO errors and assert that also no errors are returned from the function tested",
			fields: fields{
				companyRepo:   mock_domain.NewMockCompanyRepositoryInt(ctrl),
				eventProducer: mock_event.NewMockProducerInterface(ctrl),
			},
			wantErr:             assert.NoError,
			companyRepoResError: nil,
			timesEventProduced:  1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := Service{
				companyRepo:   tt.fields.companyRepo,
				eventProducer: tt.fields.eventProducer,
			}

			s.companyRepo.(*mock_domain.MockCompanyRepositoryInt).
				EXPECT().
				Delete(gomock.Any()).
				Times(1).
				Return(tt.companyRepoResError)

			s.eventProducer.(*mock_event.MockProducerInterface).
				EXPECT().
				ProduceMessageAsync(gomock.Any(), gomock.Any()).
				Times(tt.timesEventProduced).
				Return()

			tt.wantErr(t, s.DeleteCompany(tt.args.companyID), fmt.Sprintf("DeleteCompany(%v)", tt.args.companyID))
		})
	}
}

func TestService_GetCompany(t *testing.T) {
	ctrl := gomock.NewController(t)
	type fields struct {
		companyRepo   domain.CompanyRepositoryInt
		eventProducer event.ProducerInterface
	}
	type args struct {
		id string
	}
	tests := []struct {
		name                string
		fields              fields
		args                args
		want                *Company
		wantErr             assert.ErrorAssertionFunc
		companyRepoRes      *domain.Company
		companyRepoResError error
	}{
		{
			name: "we will mock companyRepo response. We will set it to return error",
			fields: fields{
				companyRepo: mock_domain.NewMockCompanyRepositoryInt(ctrl),
			},
			want:                nil,
			wantErr:             assert.Error,
			companyRepoRes:      nil,
			companyRepoResError: errors.New("someRandomError"),
		},
		{
			name: "we will mock companyRepo response. We will set it to return a company without errors",
			fields: fields{
				companyRepo: mock_domain.NewMockCompanyRepositoryInt(ctrl),
			},
			want:                nil,
			wantErr:             assert.NoError,
			companyRepoRes:      &domain.Company{},
			companyRepoResError: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := Service{
				companyRepo:   tt.fields.companyRepo,
				eventProducer: tt.fields.eventProducer,
			}

			s.companyRepo.(*mock_domain.MockCompanyRepositoryInt).
				EXPECT().
				Get(gomock.Any()).
				Times(1).
				Return(tt.companyRepoRes, tt.companyRepoResError)

			got, err := s.GetCompany(tt.args.id)
			if !tt.wantErr(t, err, fmt.Sprintf("GetCompany(%v)", tt.args.id)) {
				return
			}
			assert.IsTypef(t, tt.want, got, "GetCompany(%v)", tt.args.id)
		})
	}
}

func TestService_PatchCompany(t *testing.T) {
	ctrl := gomock.NewController(t)
	type fields struct {
		companyRepo   domain.CompanyRepositoryInt
		eventProducer event.ProducerInterface
	}
	type args struct {
		companyID string
		company   *Company
	}
	tests := []struct {
		name                string
		fields              fields
		args                args
		wantErr             assert.ErrorAssertionFunc
		companyRepoResError error
		timesEventProduced  int
	}{
		{
			name: "we will mock companyRepo.Patch() response. We will set it to return ERROR so PatchCompany will also return error",
			fields: fields{
				companyRepo:   mock_domain.NewMockCompanyRepositoryInt(ctrl),
				eventProducer: mock_event.NewMockProducerInterface(ctrl),
			},
			companyRepoResError: errors.New("some error"),
			wantErr:             assert.Error,
		},
		{
			name: "we will mock companyRepo.Patch() response. We will set it to return no ERROR so PatchCompany will" +
				" also return no error and check that ProduceMessageAsync will be called once",
			fields: fields{
				companyRepo:   mock_domain.NewMockCompanyRepositoryInt(ctrl),
				eventProducer: mock_event.NewMockProducerInterface(ctrl),
			},
			companyRepoResError: nil,
			wantErr:             assert.NoError,
			timesEventProduced:  1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := Service{
				companyRepo:   tt.fields.companyRepo,
				eventProducer: tt.fields.eventProducer,
			}

			s.companyRepo.(*mock_domain.MockCompanyRepositoryInt).
				EXPECT().
				Patch(gomock.Any(), gomock.Any()).
				Times(1).
				Return(tt.companyRepoResError)

			s.eventProducer.(*mock_event.MockProducerInterface).
				EXPECT().
				ProduceMessageAsync(gomock.Any(), gomock.Any()).
				Times(tt.timesEventProduced).
				Return()

			tt.wantErr(t, s.PatchCompany(tt.args.companyID, tt.args.company), fmt.Sprintf("PatchCompany(%v, %v)", tt.args.companyID, tt.args.company))
		})
	}
}

func TestNewService(t *testing.T) {
	ctrl := gomock.NewController(t)
	type args struct {
		companyRepo   domain.CompanyRepositoryInt
		eventProducer event.ProducerInterface
	}
	tests := []struct {
		name string
		args args
		want CompanyServiceInt
	}{
		{
			name: "no actual reason to test this. Will be tested just for coverage",
			args: args{
				companyRepo:   mock_domain.NewMockCompanyRepositoryInt(ctrl),
				eventProducer: mock_event.NewMockProducerInterface(ctrl),
			},
			want: &Service{
				companyRepo:   mock_domain.NewMockCompanyRepositoryInt(ctrl),
				eventProducer: mock_event.NewMockProducerInterface(ctrl),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, NewService(tt.args.companyRepo, tt.args.eventProducer), "NewService(%v, %v)", tt.args.companyRepo, tt.args.eventProducer)
		})
	}
}
