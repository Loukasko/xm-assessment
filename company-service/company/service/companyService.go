package service

import (
	"encoding/json"
	"github.com/google/uuid"
	"time"
	"xm-assessment/company/domain"
	"xm-assessment/event"
)

const mutationTopic = "company-mutation"
const microserviceName = "company-service"

type CompanyServiceInt interface {
	GetCompany(id string) (*Company, error)
	CreateCompany(company *Company) (string, error)
	PatchCompany(id string, company *Company) error
	DeleteCompany(id string) error
}

type Company struct {
	ID                string `json:"id,omitempty"`
	Name              string `json:"name" validate:"required,max=15"`
	Description       string `json:"description,omitempty" validate:"max=3000"`
	AmountOfEmployees int    `json:"amountOfEmployees" validate:"required"`
	IsRegistered      bool   `json:"isRegistered"`
	Type              string `json:"type" validate:"required,oneof='Corporations' 'NonProfit' 'Cooperative' 'Sole Proprietorship'"`
}

type Service struct {
	companyRepo   domain.CompanyRepositoryInt
	eventProducer event.ProducerInterface
}

func NewService(companyRepo domain.CompanyRepositoryInt, eventProducer event.ProducerInterface) CompanyServiceInt {
	return &Service{
		companyRepo:   companyRepo,
		eventProducer: eventProducer,
	}
}

func (s Service) GetCompany(id string) (*Company, error) {
	serviceCompany := &Company{}

	company, err := s.companyRepo.Get(id)
	if err != nil {
		_, isNotFoundError := err.(*domain.NotFoundError)
		if isNotFoundError {
			return nil, &NotFoundError{
				ErrorMessage: err.Error(),
			}
		}
		return nil, err
	}

	domainCompanyJSON, err := json.Marshal(company)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(domainCompanyJSON, serviceCompany)
	if err != nil {
		return nil, err
	}

	return serviceCompany, nil
}

func (s Service) CreateCompany(company *Company) (string, error) {
	domainCompany := &domain.Company{}

	serviceCompany, err := json.Marshal(company)
	if err != nil {
		return "", err
	}

	err = json.Unmarshal(serviceCompany, domainCompany)
	if err != nil {
		return "", err
	}

	domainCompany.ID = uuid.New().String()

	companyID, err := s.companyRepo.Create(domainCompany)
	if err != nil {
		return "", err
	}

	s.eventProducer.ProduceMessageAsync(mutationTopic, event.KafkaLog{
		Key:         companyID,
		ServiceName: microserviceName,
		Timestamp:   time.Now(),
		MessageType: "CREATE company requested",
		Message: struct {
			CompanyCreated *domain.Company `json:"companyCreated"`
		}{
			CompanyCreated: domainCompany,
		},
	})

	return companyID, nil
}

func (s Service) PatchCompany(companyID string, company *Company) error {
	domainCompany := &domain.Company{}

	serviceCompany, err := json.Marshal(company)
	if err != nil {
		return err
	}

	err = json.Unmarshal(serviceCompany, domainCompany)
	if err != nil {
		return err
	}

	err = s.companyRepo.Patch(companyID, domainCompany)
	if err != nil {
		_, isNotFoundError := err.(*domain.NotFoundError)
		if isNotFoundError {
			return &NotFoundError{
				ErrorMessage: err.Error(),
			}
		}
		return err
	}

	s.eventProducer.ProduceMessageAsync(mutationTopic, event.KafkaLog{
		Key:         companyID,
		ServiceName: microserviceName,
		Timestamp:   time.Now(),
		MessageType: "PATCH company requested",
		Message: struct {
			CompanyChanges *domain.Company `json:"companyChanges"`
		}{
			CompanyChanges: domainCompany,
		},
	})

	return nil
}

func (s Service) DeleteCompany(companyID string) error {
	err := s.companyRepo.Delete(companyID)
	if err != nil {
		_, isNotFoundError := err.(*domain.NotFoundError)
		if isNotFoundError {
			return &NotFoundError{
				ErrorMessage: err.Error(),
			}
		}
		return err
	}

	s.eventProducer.ProduceMessageAsync(mutationTopic, event.KafkaLog{
		Key:         companyID,
		ServiceName: microserviceName,
		Timestamp:   time.Now(),
		MessageType: "DELETE company requested",
		Message: struct {
			DeletedCompanyID string `json:"deletedCompanyID"`
		}{
			DeletedCompanyID: companyID,
		},
	})

	return nil
}
