package auth

import (
	"encoding/json"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"net/http"
	"os"
	"strings"
	"time"
)

const (
	signingMethodError = "unexpected signing method:"
)

// CreateAuthToken FOR GENERATING TEST TOKENS
func CreateAuthToken(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	token, _ := GenerateToken(os.Getenv("API_SECRET"), 12*30*24*time.Hour)
	_ = json.NewEncoder(w).Encode(struct {
		Token string `json:"token"`
	}{
		Token: token,
	})
}

func GenerateToken(secretKey string, expiration time.Duration) (string, error) {
	claims := jwt.MapClaims{}
	claims["authorized"] = true
	claims["exp"] = time.Now().Add(expiration).Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	signedToken, err := token.SignedString([]byte(secretKey))
	if err != nil {
		return "", JwtTokenCreationError{errorMessage: err.Error()}
	}
	return signedToken, nil

}

func validateToken(r *http.Request) error {
	tokenString := parseToken(r)

	_, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf(signingMethodError+"%v", token.Header["alg"])
		}

		return []byte(os.Getenv("API_SECRET")), nil
	})

	if err != nil {
		return err
	}

	return nil
}

func parseToken(r *http.Request) string {
	keys := r.URL.Query()

	token := keys.Get("token")
	if token != "" {
		return token
	}

	bearerToken := r.Header.Get("Authorization")
	if len(strings.Split(bearerToken, " ")) == 2 {
		return strings.Split(bearerToken, " ")[1]
	}

	return ""
}

type JwtTokenCreationError struct {
	errorMessage string
}

func (err JwtTokenCreationError) Error() string {
	return fmt.Sprintf("Error creating jwt token. Error:  %s", err.errorMessage)
}
