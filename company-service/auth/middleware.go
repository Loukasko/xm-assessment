package auth

import (
	"encoding/json"
	"net/http"
)

const (
	unauthorizedErrorMessage = "unauthorized"
)

func AuthenticateToken(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := validateToken(r)
		if err != nil {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusUnauthorized)
			_ = json.NewEncoder(w).Encode(struct {
				ErrorMessage string `json:"errorMessage"`
			}{
				ErrorMessage: unauthorizedErrorMessage,
			})
			return
		}

		next(w, r)
	}
}
