package main

import (
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strings"
	companyPresentation "xm-assessment/company/presentation"
	companyService "xm-assessment/company/service"
	kafka_producer "xm-assessment/kafka"
	"xm-assessment/logger"
	"xm-assessment/mongo"
	"xm-assessment/server"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		logrus.Fatal(err)
	}

	logger.Configure(os.Getenv("ENVIRONMENT"))

	kafkaBrokers := strings.Split(os.Getenv("KAFKA_ADDRESS"), ",")
	asyncKafkaProducer := kafka_producer.AsyncInit(
		os.Getenv("KAFKA_CLIENT_ID"),
		os.Getenv("ENVIRONMENT"),
		os.Getenv("KAFKA_USERNAME"),
		os.Getenv("KAFKA_PASSWORD"),
		os.Getenv("KAFKA_SASL_MECHANISM"),
		kafkaBrokers,
	)

	companyService := companyService.NewService(
		mongo.NewCompanyRepository(os.Getenv("MONGO_CONNECTION_STRING")),
		asyncKafkaProducer,
	)

	httpServer := &server.Server{
		Router: mux.NewRouter(),
		HTTPServer: &companyPresentation.CompanyHttpServer{
			CompanyService:   companyService,
			ValidatorService: validator.New(),
		},
	}

	httpHandler := httpServer.Initialize()

	err = http.ListenAndServe(`:`+os.Getenv("HTTP_LISTENER_PORT"), httpHandler)
	if err != nil {
		logrus.Errorf("error returned from http listenAndServe function, %s", err.Error())
		return
	}
}
