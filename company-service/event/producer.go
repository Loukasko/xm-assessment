package event

import (
	"github.com/Shopify/sarama"
	"time"
)

type Producer struct {
	sarama.AsyncProducer
}

type ProducerInterface interface {
	ProduceMessageAsync(topic string, log KafkaLog)
}

type KafkaLog struct {
	Key         string      `json:"messageKey"`
	ServiceName string      `json:"serviceName"`
	Timestamp   time.Time   `json:"timestamp"`
	MessageType string      `json:"messageType"`
	Message     interface{} `json:"message"`
}
