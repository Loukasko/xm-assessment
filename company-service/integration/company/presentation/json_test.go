package presentation_test

import (
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	companyPresentation "xm-assessment/company/presentation"
	companyService "xm-assessment/company/service"
	"xm-assessment/helper"
	kafka_producer "xm-assessment/kafka"
	"xm-assessment/mongo"
)

func getConfiguredService() companyService.CompanyServiceInt {
	kafkaBrokers := strings.Split(os.Getenv("KAFKA_ADDRESS"), ",")
	asyncKafkaProducer := kafka_producer.AsyncInit(
		os.Getenv("KAFKA_CLIENT_ID"),
		os.Getenv("ENVIRONMENT"),
		os.Getenv("KAFKA_USERNAME"),
		os.Getenv("KAFKA_PASSWORD"),
		os.Getenv("KAFKA_SASL_MECHANISM"),
		kafkaBrokers,
	)

	service := companyService.NewService(
		mongo.NewCompanyRepository(os.Getenv("MONGO_CONNECTION_STRING")),
		asyncKafkaProducer,
	)

	return service
}

func TestCompanyHttpServer_CreateCompany(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test in short mode")
	}
	type fields struct {
		CompanyService   companyService.CompanyServiceInt
		ValidatorService helper.Validator
	}
	type args struct {
		r *http.Request
	}
	tests := []struct {
		name           string
		fields         fields
		args           args
		wantStatusCode int
	}{
		{
			name: "we will test field validations - name length validation fails",
			fields: fields{
				CompanyService:   getConfiguredService(),
				ValidatorService: validator.New(),
			},
			args: args{
				r: &http.Request{
					Body: io.NopCloser(strings.NewReader(`
{
 "name": "a name with more than 15 characters",
 "description": "companyDescription",
 "amountOfEmployees": 100,
 "isRegistered": false,
 "type": "Sole Proprietorship"
}
`)),
				},
			},
			wantStatusCode: http.StatusBadRequest,
		},
		{
			name: "we will test that a company is created and that we are not receiving error status code",
			fields: fields{
				CompanyService:   getConfiguredService(),
				ValidatorService: validator.New(),
			},
			args: args{
				r: &http.Request{
					Body: io.NopCloser(strings.NewReader(`
{
 "name": "companyName100",
 "description": "companyDescription",
 "amountOfEmployees": 100,
 "isRegistered": false,
 "type": "Sole Proprietorship"
}
`)),
				},
			},
			wantStatusCode: http.StatusOK,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			recorder := httptest.NewRecorder()

			httpServer := &companyPresentation.CompanyHttpServer{
				CompanyService:   tt.fields.CompanyService,
				ValidatorService: tt.fields.ValidatorService,
			}
			httpServer.CreateCompany(recorder, tt.args.r)

			assert.Equal(t, tt.wantStatusCode, recorder.Code)

			//cleanup
			if recorder.Code == http.StatusOK {
				type CreateCompanyResponse struct {
					ID string `json:"ID"`
				}
				response := &CreateCompanyResponse{}
				responseBody, _ := io.ReadAll(recorder.Body)
				_ = json.Unmarshal(responseBody, response)
				_ = httpServer.CompanyService.DeleteCompany(response.ID)
			}
		})
	}
}

func TestCompanyHttpServer_DeleteCompany(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test in short mode")
	}
	type fields struct {
		CompanyService   companyService.CompanyServiceInt
		ValidatorService helper.Validator
	}
	type args struct {
		r *http.Request
	}
	tests := []struct {
		name           string
		fields         fields
		args           args
		wantStatusCode int
	}{
		{
			name: "we will test if a company is properly deleted and status ok is returned",
			fields: fields{
				CompanyService:   getConfiguredService(),
				ValidatorService: validator.New(),
			},
			args:           args{},
			wantStatusCode: http.StatusOK,
		},
		{
			name: "we will send an invalid uuid",
			fields: fields{
				CompanyService:   getConfiguredService(),
				ValidatorService: validator.New(),
			},
			args: args{
				r: &http.Request{},
			},
			wantStatusCode: http.StatusBadRequest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			recorder := httptest.NewRecorder()
			httpServer := &companyPresentation.CompanyHttpServer{
				CompanyService:   tt.fields.CompanyService,
				ValidatorService: tt.fields.ValidatorService,
			}
			if tt.wantStatusCode == http.StatusOK {
				id, _ := httpServer.CompanyService.CreateCompany(&companyService.Company{
					ID:                "ad3ae7fb-de39-4777-843d-8fed9cca2a77",
					Name:              "ToBeDeleted",
					Description:       "WILL BE DELETED",
					AmountOfEmployees: 3,
					IsRegistered:      true,
					Type:              "Cooperative",
				})
				req, _ := http.NewRequest("DELETE", "localhost:8001/company/"+id, nil)
				tt.args.r = mux.SetURLVars(req, map[string]string{
					"id": id,
				})
			}
			httpServer.DeleteCompany(recorder, tt.args.r)
			assert.Equal(t, tt.wantStatusCode, recorder.Code)
		})
	}
}

func TestCompanyHttpServer_GetCompany(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test in short mode")
	}
	type fields struct {
		CompanyService   companyService.CompanyServiceInt
		ValidatorService helper.Validator
	}
	type args struct {
		r *http.Request
	}
	tests := []struct {
		name           string
		fields         fields
		args           args
		wantStatusCode int
	}{
		{
			name: "we will create a company for testing get . We will delete it for cleanup",
			fields: fields{
				CompanyService:   getConfiguredService(),
				ValidatorService: validator.New(),
			},
			wantStatusCode: http.StatusOK,
		},
		{
			name: "the company we request will not be found",
			fields: fields{
				CompanyService:   getConfiguredService(),
				ValidatorService: validator.New(),
			},
			wantStatusCode: http.StatusNotFound,
		},
		{
			name: "the uuid we will provide will be invalid",
			fields: fields{
				CompanyService:   getConfiguredService(),
				ValidatorService: validator.New(),
			},
			args: args{
				r: &http.Request{},
			},
			wantStatusCode: http.StatusBadRequest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			recorder := httptest.NewRecorder()
			httpServer := &companyPresentation.CompanyHttpServer{
				CompanyService:   tt.fields.CompanyService,
				ValidatorService: tt.fields.ValidatorService,
			}

			if tt.wantStatusCode == http.StatusOK {
				id, _ := httpServer.CompanyService.CreateCompany(&companyService.Company{
					ID:                "ad3ae7fb-de39-4777-843d-8fed9cca2a77",
					Name:              "ToBeDeleted",
					Description:       "WILL BE DELETED",
					AmountOfEmployees: 3,
					IsRegistered:      true,
					Type:              "Cooperative",
				})
				req, _ := http.NewRequest("GET", "localhost:8001/company/"+id, nil)
				tt.args.r = mux.SetURLVars(req, map[string]string{
					"id": id,
				})
			}

			if tt.wantStatusCode == http.StatusNotFound {
				req, _ := http.NewRequest("GET", "localhost:8001/company/9ac71b62-f54b-4f33-b00e-dbe2ff57e72f", nil)
				tt.args.r = mux.SetURLVars(req, map[string]string{
					"id": "9ac71b62-f54b-4f33-b00e-dbe2ff57e72f",
				})
			}

			httpServer.GetCompany(recorder, tt.args.r)
			assert.Equal(t, tt.wantStatusCode, recorder.Code)

			if tt.wantStatusCode == http.StatusOK {
				_ = httpServer.CompanyService.DeleteCompany(mux.Vars(tt.args.r)["id"])
			}
		})
	}
}

func TestCompanyHttpServer_PatchCompany(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test in short mode")
	}
	type fields struct {
		CompanyService   companyService.CompanyServiceInt
		ValidatorService helper.Validator
	}
	type args struct {
		r *http.Request
	}
	tests := []struct {
		name           string
		fields         fields
		args           args
		wantStatusCode int
	}{
		{
			name: "we will create a company for testing patch . We will delete it for cleanup",
			fields: fields{
				CompanyService:   getConfiguredService(),
				ValidatorService: validator.New(),
			},
			wantStatusCode: http.StatusOK,
		},
		{
			name: "the company we request will not be found",
			fields: fields{
				CompanyService:   getConfiguredService(),
				ValidatorService: validator.New(),
			},
			wantStatusCode: http.StatusNotFound,
		},
		{
			name: "the uuid we will provide will be invalid",
			fields: fields{
				CompanyService:   getConfiguredService(),
				ValidatorService: validator.New(),
			},
			args: args{
				r: &http.Request{},
			},
			wantStatusCode: http.StatusBadRequest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			recorder := httptest.NewRecorder()
			httpServer := &companyPresentation.CompanyHttpServer{
				CompanyService:   tt.fields.CompanyService,
				ValidatorService: tt.fields.ValidatorService,
			}

			if tt.wantStatusCode == http.StatusOK {
				id, _ := httpServer.CompanyService.CreateCompany(&companyService.Company{
					ID:                "ad3ae7fb-de39-4777-843d-8fed9cca2a77",
					Name:              "ToBeDeleted",
					Description:       "WILL BE DELETED",
					AmountOfEmployees: 3,
					IsRegistered:      true,
					Type:              "Cooperative",
				})
				req, _ := http.NewRequest("PATCH", "localhost:8001/company/"+id, nil)
				tt.args.r = mux.SetURLVars(req, map[string]string{
					"id": id,
				})

				tt.args.r.Body = io.NopCloser(strings.NewReader(`
{
 "description": "NEW companyDescription"
}
`))
			}

			if tt.wantStatusCode == http.StatusNotFound {
				req, _ := http.NewRequest("GET", "localhost:8001/company/9ac71b62-f54b-4f33-b00e-dbe2ff57e72f", nil)
				tt.args.r = mux.SetURLVars(req, map[string]string{
					"id": "9ac71b62-f54b-4f33-b00e-dbe2ff57e72f",
				})
				tt.args.r.Body = io.NopCloser(strings.NewReader(`
{
 "description": "NEW companyDescription"
}
`))
			}

			httpServer.PatchCompany(recorder, tt.args.r)
			assert.Equal(t, tt.wantStatusCode, recorder.Code)

			if tt.wantStatusCode == http.StatusOK {
				_ = httpServer.CompanyService.DeleteCompany(mux.Vars(tt.args.r)["id"])
			}
		})
	}
}
