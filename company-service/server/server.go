package server

import (
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"net/http"
	"xm-assessment/auth"
	companyPresentation "xm-assessment/company/presentation"
)

type Server struct {
	Router     *mux.Router
	HTTPServer *companyPresentation.CompanyHttpServer
}

func (s *Server) Initialize() http.Handler {
	s.Router = mux.NewRouter()
	s.route()

	headersOk := handlers.AllowedHeaders([]string{"Content-Type", "Authorization"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"POST", "PATCH", "DELETE", "GET"})
	allowCredentials := handlers.AllowCredentials()
	return handlers.CORS(originsOk, headersOk, methodsOk, allowCredentials)(s.Router)
}

func (s *Server) route() {
	s.Router.HandleFunc("/company/{id}", s.HTTPServer.GetCompany).Methods(http.MethodGet)
	s.Router.HandleFunc("/company", auth.AuthenticateToken(s.HTTPServer.CreateCompany)).Methods(http.MethodPost)
	s.Router.HandleFunc("/company/{id}", auth.AuthenticateToken(s.HTTPServer.PatchCompany)).Methods(http.MethodPatch)
	s.Router.HandleFunc("/company/{id}", auth.AuthenticateToken(s.HTTPServer.DeleteCompany)).Methods(http.MethodDelete)

	//FOR ASSESSMENT'S TESTING PURPOSES
	s.Router.HandleFunc("/token", auth.CreateAuthToken).Methods(http.MethodGet)

}
