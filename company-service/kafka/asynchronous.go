package kafka_producer

import (
	"crypto/tls"
	"encoding/json"
	"github.com/Shopify/sarama"
	"github.com/sirupsen/logrus"
	"log"
	"os"
	"sync"
	"time"
	"xm-assessment/event"
)

var once sync.Once

type Async struct {
	clientID      string
	brokerList    []string
	environment   string
	username      string
	password      string
	saslMechanism string
}

func AsyncInit(clientID, environment, username, password, saslMechanism string, brokerList []string) *Async {
	return &Async{
		clientID:      clientID,
		brokerList:    brokerList,
		environment:   environment,
		username:      username,
		password:      password,
		saslMechanism: saslMechanism,
	}
}

var kafkaProducer event.Producer

func (a *Async) ProduceMessageAsync(topic string, value event.KafkaLog) {
	JSONLog, _ := json.Marshal(value)
	p := a.getAsyncProducer()
	if p.AsyncProducer == nil {
		logrus.Errorf("sarama producer is not initialized")
		return
	}

	message := &sarama.ProducerMessage{
		Topic:     topic,
		Key:       sarama.StringEncoder(value.Key),
		Value:     sarama.StringEncoder(JSONLog),
		Timestamp: time.Now(),
	}
	p.AsyncProducer.Input() <- message
}

func (a *Async) getAsyncProducer() event.Producer {
	once.Do(func() {
		config := sarama.NewConfig()
		if a.environment == "staging" || a.environment == "production" {
			config.Net.SASL.Enable = true
			config.Net.SASL.User = a.username
			config.Net.SASL.Password = a.password
			config.Net.SASL.Handshake = true
			config.Net.SASL.SCRAMClientGeneratorFunc = func() sarama.SCRAMClient { return &XDGSCRAMClient{HashGeneratorFcn: SHA512} }
			config.Net.SASL.Mechanism = sarama.SASLTypeSCRAMSHA512

			tlsConfig := tls.Config{}
			config.Net.TLS.Enable = true
			config.Net.TLS.Config = &tlsConfig
		}
		config.Producer.Compression = sarama.CompressionSnappy // Compress messages
		config.Producer.MaxMessageBytes = 10000000
		config.Producer.Flush.Frequency = 500 * time.Millisecond // Flush batches every 500ms
		config.ClientID = a.clientID
		config.Version = sarama.V3_2_0_0
		producer, err := sarama.NewAsyncProducer(a.brokerList, config)
		if err != nil {
			logrus.Errorf("Failed to start Sarama producer: %s", err.Error())
		}

		// We will just log to STDOUT if we're not able to produce messages.
		// Note: messages will only be returned here after all retry attempts are exhausted.
		go func() {
			for err := range producer.Errors() {
				logrus.Errorf("Failed to write access log entry: %s", err.Error())
			}
		}()

		sarama.Logger = log.New(os.Stdout, "[sarama] ", log.LstdFlags)
		kafkaProducer.AsyncProducer = producer
	})
	return kafkaProducer
}
