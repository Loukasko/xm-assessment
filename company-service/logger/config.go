package logger

import (
	"github.com/sirupsen/logrus"
)

func Configure(environment string) {
	var minimumLogLevel logrus.Level

	if environment == "production" {
		minimumLogLevel = logrus.WarnLevel
	} else {
		minimumLogLevel = logrus.TraceLevel
	}

	logrus.SetReportCaller(true)
	logrus.SetLevel(minimumLogLevel)
}
