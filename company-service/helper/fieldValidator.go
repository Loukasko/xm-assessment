package helper

type Validator interface {
	Struct(s interface{}) error
}
